var React = require('react');

var Counter = React.createClass({
	getInitialState(){
		return {
			value:0
		}
	},
	render(){
		return (
				<div>
					<h1>{this.state.value}</h1>
					<button onClick={this.decrement}> - </button>
					<button onClick={this.increment}> + </button>
				</div>
			);
	},
	decrement(){
		this.setState({value:this.state.value-1});
	},
	increment(){
		this.setState({value:this.state.value+1});
	}

});

module.exports = Counter;