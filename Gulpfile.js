var gulp 		= require('gulp'),
	browserify 	= require('gulp-browserify'),
	es6 		= require('gulp-es6-transpiler'),
	uglify 		= require('gulp-uglify'),
	argv 		= require('yargs').argv;



var source_directory = './src/';

gulp.task('js', function(){
	
	var stream = gulp.src(source_directory+"*.js")
					.pipe(browserify({
						transform:['reactify']
					}));

	if(argv.vanilla)				
	{
		stream = stream.pipe(es6({
			globals:{
				define:true
			}
		}));	
	}


	if(argv.minify)
	{
		stream = stream.pipe(uglify());
	}


	stream.pipe(gulp.dest('./js'))

});


gulp.task('watch', function(){
	gulp.watch(source_directory+"**/*.js", ['js']);
});

gulp.task('default', ['js','watch']);