#Gulp, Browserify & React boilerplate

To get started, run `npm install`. You might want to also run `npm install -g gulp`. 

When finished, just run `gulp`; it'll detect changes in the src/ folder and compile javascript to the /js directory.